Name:           spice-vdagent
Version:        0.22.1
Release:        2
Summary:        Agent for Spice guests
License:        GPLv3+
URL:            https://spice-space.org/
Source0:        https://spice-space.org/download/releases/%{name}-%{version}.tar.bz2

Patch0:         backport-Fix-minor-spelling-errors.patch

BuildRequires:  systemd-devel
BuildRequires:  glib2-devel >= 2.50
BuildRequires:  spice-protocol >= 0.14.3
BuildRequires:  libpciaccess-devel libXrandr-devel libXinerama-devel
BuildRequires:  libXfixes-devel systemd desktop-file-utils libtool
BuildRequires:  alsa-lib-devel dbus-devel libdrm-devel
# For autoreconf, needed after clipboard patch series
BuildRequires:  automake autoconf

%{?systemd_requires}

%description
%{name} is an optional component for enhancing user experience and performing guest-oriented management tasks. 
Its features includes: client mouse mode (no need to grab mouse by client, no mouse lag), automatic adjustment 
of screen resolution, copy and paste (text and image) between client and domU. It also requires vdagent service 
installed on domU o.s. to work. The default is 0.
  
%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-session-info=systemd --with-init-script=systemd
%make_build  V=2

%install
make install DESTDIR=$RPM_BUILD_ROOT V=2
%define _unpackaged_files_terminate_build 0

%post
%systemd_post spice-vdagentd.service spice-vdagentd.socket

%preun
%systemd_preun spice-vdagentd.service spice-vdagentd.socket

%postun
%systemd_postun_with_restart spice-vdagentd.service spice-vdagentd.socket

%files
%defattr(-,root,root)
%license  COPYING 
%{_bindir}/spice-vdagent
%{_sbindir}/spice-vdagentd
%{_prefix}/lib/systemd/system/spice-vdagentd.service
%{_prefix}/lib/systemd/system/spice-vdagentd.socket
%{_prefix}/lib/tmpfiles.d/spice-vdagentd.conf
%{_prefix}/lib/udev/rules.d/70-spice-vdagentd.rules
%{_datadir}/gdm/autostart/LoginWindow/spice-vdagent.desktop
%{_datadir}/gdm/greeter/autostart/spice-vdagent.desktop
%{_sysconfdir}/xdg/autostart/spice-vdagent.desktop
%dir %{_var}/run/spice-vdagentd

%files help
%defattr(-,root,root)
%doc COPYING CHANGELOG.md README.md
%{_mandir}/man1/*.1.gz

%changelog
* Wed Jul 31 2024 lingsheng <lingsheng1@h-partners.com> - 0.22.1-2
- Fix minor spelling errors

* Sun Jul 31 2022 tianlijing <tianlijing@kylinos.cn> - 0.22.1-1
- upgrade to 0.22.1

* Fri Dec 3 2021 yangcheng <yangcheng87@huawei.com> - 0.21.0-1
- upgrade to 0.21.0

* Mon Feb 22 2021 jinzhimin <jinzhimin2@huawei.com> - 0.20.0-2
- fix CVE-2020-25650 CVE-2020-25651 CVE-2020-25652 CVE-2020-25653

* Fri Jul 17 2020 chengguipeng <chengguipeng1@huawei.com> - 0.20.0-1
- upgrade to 0.20.0

* Fri Oct 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.0-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Sat Oct 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.0-3
- Package init

